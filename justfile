# Run pytest unit tests. Note that this will attempt to start a MongoDB 4.0 server locally on port 27017 via Docker
test +COMMAND='':
    cargo build
    RUST_BACKTRACE=1 pytest {{COMMAND}}

# Format Nix & Python code
format:
    nixfmt *.nix
    black .
    isort .

# Start a MongoDB 4.0 server (requires Docker)
mongod:
    docker run --rm -it -p 27017:27017 -v /tmp/mongo:/data/db mongo:4.0 --noauth
    