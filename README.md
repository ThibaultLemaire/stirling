# Stirling

A WIP async MongoDB client for Python, based on the official client for Rust.

Right now, this is only to dogfood [my approach for Rust->Python async compatibility](https://gitlab.com/ThibaultLemaire/pyo3-futures).

## Development

### Dependencies

[Nix](https://nixos.org/download.html).

(Or you can try to figure them out yourself by looking at the [`shell.nix`](shell.nix) file.)

### Getting Started

```sh
nix-shell
just --list
```

## Why that name?

Because a Stirling engine is a Rust-ier [motor](https://github.com/mongodb/motor).
