use {
    futures::stream::StreamExt,
    mongodb::bson,
    pyo3::{
        class::{
            basic::{CompareOp, PyObjectProtocol},
            mapping::PyMappingProtocol,
            sequence::PySequenceProtocol,
        },
        create_exception,
        exceptions::{PyException, PyKeyError, PyNotImplementedError, PyTypeError},
        prelude::*,
        types::PyDict,
    },
    pyo3_futures::{PyAsync, PyAsyncGen},
    std::{
        collections::hash_map::DefaultHasher,
        hash::{Hash, Hasher},
    },
};

enum StirlingError {
    MongoError(mongodb::error::Error),
}

impl From<mongodb::error::Error> for StirlingError {
    fn from(error: mongodb::error::Error) -> Self {
        Self::MongoError(error)
    }
}

create_exception!(stirling, StirlingException, PyException);

impl From<StirlingError> for PyErr {
    fn from(error: StirlingError) -> Self {
        use StirlingError::*;

        match error {
            MongoError(error) => StirlingException::new_err(error.to_string()),
        }
    }
}

type StirlingResult<T> = Result<T, StirlingError>;

#[pyclass(module = "stirling")]
struct ClientOptions {
    options: mongodb::options::ClientOptions,
}

#[pymethods]
impl ClientOptions {
    #[staticmethod]
    fn parse(s: String) -> PyAsync<StirlingResult<Self>> {
        async move {
            Ok(Self {
                options: mongodb::options::ClientOptions::parse(&s).await?,
            })
        }
        .into()
    }

    #[getter]
    fn get_app_name(&self) -> Option<&String> {
        self.options.app_name.as_ref()
    }
    #[setter]
    fn set_app_name(&mut self, value: Option<String>) {
        self.options.app_name = value
    }
}

#[pyclass(module = "stirling")]
struct Client {
    client: mongodb::Client,
}

#[pymethods]
impl Client {
    #[staticmethod]
    fn with_options(options: &ClientOptions) -> StirlingResult<Self> {
        Ok(Self {
            client: mongodb::Client::with_options(options.options.clone())?,
        })
    }

    fn list_database_names(&self) -> PyAsync<StirlingResult<Vec<String>>> {
        let client = self.client.clone();
        async move { Ok(client.list_database_names(None, None).await?) }.into()
    }

    fn database(&self, name: &str) -> Database {
        Database {
            db: self.client.database(name),
        }
    }
}

#[pyclass(module = "stirling")]
struct Database {
    db: mongodb::Database,
}

#[pymethods]
impl Database {
    fn collection(&self, name: &str) -> Collection {
        Collection {
            collection: self.db.collection(name),
        }
    }
}

#[pyclass(module = "stirling")]
struct Collection {
    collection: mongodb::Collection,
}

#[pymethods]
impl Collection {
    fn insert_one(&self, doc: Document) -> PyAsync<StirlingResult<InsertOneResult>> {
        let collection = self.collection.clone();
        async move { Ok(collection.insert_one(doc.document, None).await?.into()) }.into()
    }

    fn find_one(&self, filter: Document) -> PyAsync<StirlingResult<Option<Document>>> {
        let collection = self.collection.clone();
        async move {
            Ok(collection
                .find_one(filter.document, None)
                .await?
                .map(Document::from))
        }
        .into()
    }

    fn find(
        &self,
        filter: Document,
    ) -> PyAsync<StirlingResult<PyAsyncGen<StirlingResult<Document>>>> {
        let collection = self.collection.clone();
        async move {
            Ok(collection
                .find(filter.document, None)
                .await?
                .map(|item| Ok(item?.into()))
                .into())
        }
        .into()
    }
}

#[derive(FromPyObject)]
enum PyDictLike<'a> {
    Dict(&'a PyDict),
    Iter(&'a PyAny),
}

#[pyclass(module = "stirling")]
struct Document {
    document: bson::Document,
}

#[pymethods]
impl Document {
    #[new]
    fn new(dictlike: PyDictLike) -> PyResult<Self> {
        fn from_iterator<'a, 'b>(
            iterator: impl Iterator<Item = PyResult<(&'a PyAny, &'b PyAny)>>,
        ) -> PyResult<Document> {
            iterator
                .map(|pair| {
                    pair.and_then(|(key, value)| {
                        Ok((String::extract(key)?, Bson::extract(value)?.into()))
                    })
                })
                .collect::<PyResult<bson::Document>>()
                .map(Document::from)
        }

        use PyDictLike::*;
        match dictlike {
            Dict(dict) => from_iterator(dict.iter().map(Ok)),
            Iter(iter) => from_iterator(iter.iter()?.map(|pair| pair.and_then(PyAny::extract))),
        }
    }
}

#[pyproto]
impl PyObjectProtocol for Document {
    fn __hash__(&self) -> PyResult<u8> {
        Err(PyTypeError::new_err("unhashable type: 'stirling.Document'"))
    }

    fn __richcmp__(&self, other: Self, op: CompareOp) -> PyResult<bool> {
        use CompareOp::*;
        Ok(match op {
            Eq => self.document == other.document,
            Ne => self.document != other.document,
            op => {
                return Err(PyNotImplementedError::new_err(format!(
                    "operation not supported for stirling.Document {:?}",
                    op
                )))
            }
        })
    }
}

#[pyproto]
impl PyMappingProtocol for Document {
    fn __len__(&self) -> usize {
        self.document.len()
    }

    fn __getitem__(&self, key: String) -> PyResult<Bson> {
        self.document
            .get(&key)
            .ok_or(PyKeyError::new_err(key))
            .map(|value| value.clone().into())
    }

    fn __setitem__(&mut self, key: &str, value: Bson) {
        self.document.insert(key, value);
    }

    fn __delitem__(&mut self, key: String) -> PyResult<()> {
        if let None = self.document.remove(&key) {
            Err(PyKeyError::new_err(key))
        } else {
            Ok(())
        }
    }
}

#[pyproto]
impl PySequenceProtocol for Document {
    fn __contains__(&self, key: &str) -> bool {
        self.document.contains_key(key)
    }
}

impl From<bson::Document> for Document {
    fn from(document: bson::Document) -> Self {
        Self { document }
    }
}

impl From<Document> for bson::Document {
    fn from(document: Document) -> Self {
        document.document
    }
}

impl<'source> FromPyObject<'source> for Document {
    fn extract(ob: &'source PyAny) -> PyResult<Self> {
        ob.extract::<PyRef<Self>>()
            .map(|doc| Self {
                document: doc.document.clone(),
            })
            .or(Self::new(ob.extract()?))
    }
}

#[derive(FromPyObject)]
enum NonNullBson {
    String(String),
    Int32(i32),
    ObjectId(ObjectId),
    Document(Document),
}

enum Bson {
    NonNull(NonNullBson),
    Null,
}

impl<'source> FromPyObject<'source> for Bson {
    fn extract(ob: &'source PyAny) -> PyResult<Self> {
        Ok(match ob.extract()? {
            Some(bson) => Self::NonNull(bson),
            None => Self::Null,
        })
    }
}

impl From<bson::Bson> for Bson {
    fn from(bson: bson::Bson) -> Self {
        use NonNullBson::*;
        match bson {
            bson::Bson::Null => Self::Null,
            bson::Bson::String(s) => Self::NonNull(String(s)),
            bson::Bson::Int32(i) => Self::NonNull(Int32(i)),
            bson::Bson::ObjectId(oid) => Self::NonNull(ObjectId(oid.into())),
            bson::Bson::Document(doc) => Self::NonNull(Document(doc.into())),
            _ => unimplemented!(),
        }
    }
}

impl From<Bson> for bson::Bson {
    fn from(bson: Bson) -> Self {
        use NonNullBson::*;
        match bson {
            Bson::Null => Self::Null,
            Bson::NonNull(String(s)) => Self::String(s),
            Bson::NonNull(Int32(i)) => Self::Int32(i),
            Bson::NonNull(ObjectId(oid)) => Self::ObjectId(oid.into()),
            Bson::NonNull(Document(doc)) => Self::Document(doc.into()),
        }
    }
}

impl IntoPy<PyObject> for Bson {
    fn into_py(self, py: Python) -> PyObject {
        use NonNullBson::*;
        match self {
            Self::Null => py.None(),
            Self::NonNull(String(s)) => s.into_py(py),
            Self::NonNull(Int32(i)) => i.into_py(py),
            Self::NonNull(ObjectId(oid)) => oid.into_py(py),
            Self::NonNull(Document(doc)) => doc.into_py(py),
        }
    }
}

#[pyclass(module = "stirling")]
#[derive(Clone)]
struct ObjectId {
    oid: bson::oid::ObjectId,
}

impl From<bson::oid::ObjectId> for ObjectId {
    fn from(oid: bson::oid::ObjectId) -> Self {
        Self { oid }
    }
}

impl From<ObjectId> for bson::oid::ObjectId {
    fn from(oid: ObjectId) -> Self {
        oid.oid
    }
}

#[pyproto]
impl PyObjectProtocol for ObjectId {
    fn __str__(&self) -> String {
        self.oid.to_hex()
    }

    fn __hash__(&self) -> u64 {
        let mut hasher = DefaultHasher::new();
        self.oid.hash(&mut hasher);
        hasher.finish()
    }

    fn __richcmp__(&self, other: PyRef<Self>, op: CompareOp) -> bool {
        use CompareOp::*;
        match op {
            Eq => self.oid == other.oid,
            Ne => self.oid != other.oid,
            Gt => self.oid > other.oid,
            Ge => self.oid >= other.oid,
            Lt => self.oid < other.oid,
            Le => self.oid <= other.oid,
        }
    }
}

#[pyclass(module = "stirling")]
struct InsertOneResult {
    result: mongodb::results::InsertOneResult,
}

impl From<mongodb::results::InsertOneResult> for InsertOneResult {
    fn from(result: mongodb::results::InsertOneResult) -> Self {
        Self { result }
    }
}

#[pymethods]
impl InsertOneResult {
    #[getter]
    fn get_inserted_id(&self) -> Bson {
        self.result.inserted_id.clone().into()
    }
}

#[pymodule]
fn stirling(py: Python, m: &PyModule) -> PyResult<()> {
    m.add("StirlingException", py.get_type::<StirlingException>())?;
    m.add_class::<ClientOptions>()?;
    m.add_class::<Client>()?;
    m.add_class::<Database>()?;
    m.add_class::<Collection>()?;
    m.add_class::<Document>()?;
    m.add_class::<InsertOneResult>()?;
    m.add_class::<ObjectId>()?;
    Ok(())
}
