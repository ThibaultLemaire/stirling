{ pkgs ? import <nixpkgs> { } }:

with pkgs;
mkShell {
  buildInputs = [ just rustup nixfmt ] ++ (with python3Packages; [
    python
    uvloop
    pytest
    pytest-asyncio
    black
    isort
  ]);
}
