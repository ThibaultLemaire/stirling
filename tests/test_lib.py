import asyncio
from subprocess import PIPE, Popen

import pytest

from . import stirling


@pytest.fixture(scope="session")
def mongo_daemon():
    mongod = Popen(
        [
            "docker",
            "run",
            "--rm",
            "-p",
            "27017:27017",
            "-v",
            "/tmp/mongo:/data/db",
            "mongo:4.0",
            "--noauth",
            "--syncdelay",
            "0",
            "--nojournal",
        ],
        stdout=PIPE,
        stderr=PIPE,
    )
    while True:
        assert mongod.poll() is None, "Mongo failed to start"
        assert mongod.stdout, "Mongo misconfigured"
        line = mongod.stdout.readline()
        if b"waiting for connections" in line:
            break

    yield mongod

    mongod.terminate()
    while True:
        if mongod.poll() is not None:
            break


@pytest.fixture(scope="session")
def event_loop():
    """ https://github.com/pytest-dev/pytest-asyncio/issues/38#issuecomment-264418154 """
    loop = asyncio.get_event_loop()
    yield loop
    loop.close()


@pytest.fixture(scope="session")
async def client_options():
    return await stirling.ClientOptions.parse("mongodb://localhost:27017")


@pytest.fixture(scope="session")
def client(client_options, mongo_daemon):
    assert mongo_daemon.poll() is None, "Mongo crashed"
    return stirling.Client.with_options(client_options)


@pytest.fixture(scope="session")
def database(client):
    return client.database("SCP")


@pytest.fixture(scope="session")
def collection(database):
    return database.collection("objects")


@pytest.mark.asyncio
async def test_clientoptions__parse__ok():
    rustine = stirling.ClientOptions.parse("mongodb://localhost:27017")
    assert str(type(rustine)) == "<class 'pyo3_futures.Rustine'>"

    client_options = await rustine

    assert isinstance(client_options, stirling.ClientOptions)
    assert stirling.ClientOptions.__module__ == "stirling"


@pytest.mark.asyncio
async def test_clientoptions__parse__err():

    with pytest.raises(
        stirling.StirlingException,
        match=r"An invalid argument was provided to a database operation: connection string contains no scheme",
    ):
        await stirling.ClientOptions.parse("not a mongo uri")


@pytest.mark.asyncio
async def test_clientoptions__app_name(client_options: stirling.ClientOptions):

    client_options.app_name = "Faithful OS"

    assert client_options.app_name == "Faithful OS"


def test_document__new__dict():

    document = stirling.Document({"Item #": 173, "Object Class": "Euclid"})

    assert isinstance(document, stirling.Document)


def test_document__new__dict_items():

    document = stirling.Document({"Item #": "093", "Object Class": "Euclid"}.items())

    assert isinstance(document, stirling.Document)


def test_document__new__list():

    document = stirling.Document([("Item #", "087"), ("Object Class", "Euclid")])

    assert isinstance(document, stirling.Document)


def test_document__new__generator():
    def gen_doc():
        yield ("Item #", "096")
        yield ("Object Class", "Euclid")

    document = stirling.Document(gen_doc())

    assert isinstance(document, stirling.Document)


def test_document__is_not_hashable():
    document = stirling.Document({})

    with pytest.raises(TypeError, match="unhashable type: 'stirling.Document'"):
        hash(document)


def test_document__richcomp():
    base_dict = {"Item #": "SCP-073", "Object Class": "Euclid"}
    ref_to_base_dict = base_dict
    identical_dict = {"Item #": "SCP-073", "Object Class": "Euclid"}
    different_dict = {"Item #": "SCP-076", "Object Class": "Keter"}
    assert ref_to_base_dict is base_dict
    assert identical_dict is not base_dict
    assert base_dict == ref_to_base_dict == identical_dict
    assert different_dict != base_dict

    doc_from_base_dict = stirling.Document(base_dict)
    ref_to_doc_from_base_dict = doc_from_base_dict
    other_doc_from_base_dict = stirling.Document(base_dict)
    doc_from_identical_dict = stirling.Document(identical_dict)
    doc_from_different_dict = stirling.Document(different_dict)

    assert ref_to_doc_from_base_dict is doc_from_base_dict
    assert other_doc_from_base_dict is not doc_from_base_dict
    assert (
        doc_from_base_dict
        == ref_to_doc_from_base_dict
        == other_doc_from_base_dict
        == doc_from_identical_dict
    )
    assert doc_from_different_dict != doc_from_base_dict


def test_document__pymappingprotocol():
    document = stirling.Document({})
    with pytest.raises(KeyError, match="Item #"):
        document["Item #"]
    with pytest.raises(KeyError, match="Item #"):
        del document["Item #"]

    document["Item #"] = "SCP-055"
    assert document["Item #"] == "SCP-055"
    del document["Item #"]

    with pytest.raises(KeyError, match="Item #"):
        document["Item #"]


@pytest.mark.asyncio
async def test_client__with_options__ok(client_options: stirling.ClientOptions):

    client = stirling.Client.with_options(client_options)

    assert isinstance(client, stirling.Client)


@pytest.mark.asyncio
async def test_client__list_database_names__server_unreachable():
    client = stirling.Client.with_options(
        await stirling.ClientOptions.parse("mongodb://unreachable:27017")
    )

    with pytest.raises(asyncio.TimeoutError):
        await asyncio.wait_for(client.list_database_names(), timeout=0.5)


@pytest.mark.asyncio
async def test_client__list_database_names__ok(client: stirling.Client):

    db_names = await asyncio.wait_for(client.list_database_names(), timeout=0.5)

    assert isinstance(db_names, list)
    for name in db_names:
        assert isinstance(name, str)
    assert "admin" in db_names


@pytest.mark.asyncio
async def test_client__database(client: stirling.Client):

    db = client.database("SCP")

    assert isinstance(db, stirling.Database)


@pytest.mark.asyncio
async def test_database__collection(database: stirling.Database):

    collection = database.collection("objects")

    assert isinstance(collection, stirling.Collection)


@pytest.mark.parametrize(
    "document",
    (
        stirling.Document(
            {"Item #": 682, "Object Class": "Keter"},
        ),
        {"Item #": "SCP-079", "Object Class": "Euclid"},
        {
            "meta": {"Item #": "SCP-049", "Object Class": "Euclid"},
            "Description": "SCP-049 is a humanoid entity, roughly 1.9 meters in height, which bears the appearance of a medieval plague doctor.",
        },
        {"Item #": "SCP-048", "Object Class": None},
    ),
)
@pytest.mark.asyncio
async def test_collection__insert_one__and__find_one(
    collection: stirling.Collection, document
):

    result = await collection.insert_one(document)

    assert isinstance(result, stirling.InsertOneResult)
    inserted_id = result.inserted_id
    assert isinstance(inserted_id, stirling.ObjectId)
    as_str = str(inserted_id)
    assert len(as_str) == 24

    inserted = await collection.find_one({"_id": inserted_id})

    assert inserted is not None
    assert isinstance(inserted, stirling.Document)
    assert inserted["_id"] == inserted_id
    del inserted["_id"]
    assert inserted == document
    assert inserted is not document


@pytest.mark.asyncio
async def test_object_id__richcomp__and__hash(collection: stirling.Collection):
    oid, identical_oid, different_oid = (
        document["_id"]
        for document in await asyncio.gather(
            *(
                collection.find_one({"Item #": number})
                for number in ("SCP-079", "SCP-079", 682)
            )
        )
    )
    ref_to_oid = oid

    assert ref_to_oid is oid
    assert identical_oid is not oid
    assert oid == ref_to_oid == identical_oid
    assert hash(oid) == hash(ref_to_oid) == hash(identical_oid)
    assert different_oid != oid
    assert hash(different_oid) != hash(oid)
    assert oid > different_oid
    assert oid >= different_oid
    assert oid >= identical_oid
    assert oid <= identical_oid
    assert different_oid <= oid
    assert different_oid < oid
    assert not different_oid > oid
    assert not different_oid >= oid
    assert not oid <= different_oid
    assert not oid < different_oid


@pytest.mark.asyncio
async def test_collection__find_one__as_completed_is_not_ordered(
    collection: stirling.Collection,
):
    filters = ({"Item #": 682},) * 10 + ({"meta.Item #": "SCP-049"},) * 10
    rustines = [collection.find_one(filt) for filt in filters]

    for (filt, result) in zip(
        filters,
        asyncio.as_completed(rustines),
    ):
        document = await result
        assert document["_id"]
        [key] = filt.keys()
        # Does it have "Item #" when we expect "meta"? And vice-versa
        if key.split(".")[0] not in document:
            # That means we don't get the results in the order we started the queries
            return  # One is enough proof, we can break early

    assert False, "All results came out in the same order"


@pytest.mark.asyncio
async def test_collection__find(collection: stirling.Collection):

    matches = []
    async for item in (await collection.find({})):
        matches.append(item)

    assert matches
